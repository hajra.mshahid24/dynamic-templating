import { useContext, useState } from 'react';
import styles from '../styles/Home.module.css'
import { CardContext } from './Home';


const Welcome = () => {
    const {name} = useContext(CardContext);
    const temp = '{{name}}';
    return (
        // <div className={styles.card}>
            <div className={styles.bgwhite}>
                <h3>Welcome</h3>
                <h2>{name}</h2>
            </div>
            
        // </div>
    )
}

export default Welcome
