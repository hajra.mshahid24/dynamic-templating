import { useContext, useState } from 'react'
import styles from '../styles/Home.module.css'
import { CardContext } from './Home'


const Invite = () => {

    const { f_name , m_name } = useContext(CardContext);
    
    return (
            <div className={styles.bgwhite}>
                <h3>Anmol weds Prathami</h3>
                <p>With the blessings of <span className="font-medium underline">Mr. {f_name}</span> & <span className="font-medium underline">Mrs. {m_name}</span> Bhojani request the pleasure of your company on this auspicious occasion of the wedding ceremony of their beloved son.</p>
            </div>
    )
}

export default Invite
