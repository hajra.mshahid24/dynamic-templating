// import { useState } from 'react';

// export default () => {
//   const [todos, setTodos] = useState([]);
//   const [todo, setTodo] = useState('');
//   const removeTodo = todo => {
//     setTodos(todos.filter(t => t !== todo));
//   };

//   const add = (id) => {
//     if(id){
//       setTodos([...todos , {todo: todo}]);
//     setTodo('')
//     }
//   }

//   console.log(todos)
//   return (
//     <>
//       <input value={todo} onChange={e => setTodo(e.target.value)} />
//       <button onClick={() => add(2)}>Add</button>
//       {todos.map((todo, index) => (
//         <>
//           <h1>{todo.todo}</h1>
//           <button onClick={() => removeTodo(todo)}>remove</button>
//         </>
//       ))}
//     </>
//   );
// };



import Head from 'next/head'
import React , { useState , useEffect, createContext} from 'react'
import Developer from '../components/Developer';
import Invite from '../components/Invite';
import Parents from '../components/Parents';
import RSVP from '../components/RSVP';
import Schedule from '../components/Schedule';
import Venue from '../components/Venue';
import Welcome from '../components/Welcome';
import styles from '../styles/Home.module.css';
import Cards from './Cards';
import InviteInput from './InviteInput';
import WelcomeInput from './WelcomeInput';

export const CardContext = createContext(null);

function Home() {
  const [input, setInput] = useState([]);
  const [cards, setCards] = useState([]);
  const [currCard , setcurrCard] = useState('');
  const [currInput , setcurrInput] = useState('');
  const [currTitle , setcurrTitle] = useState('');
  const [name , setName] = useState('{{name}}');
  const [m_name , setMname] = useState('{{xyz}}')
  const [f_name , setFname] = useState('{{xyz}}')

  const addCard = (card) => {

    switch(card){
      case 'Welcome' : 
      console.log(';hey')
        setcurrCard(<Welcome />);
        setcurrInput(<WelcomeInput/>);
        setcurrTitle('Welcome');

        console.log("c" , currInput)
        break;
      case 'Invite' : 
        setcurrCard(<Invite />); 
        setcurrInput(<InviteInput />);
        setcurrTitle('Invite');
        break;
      case 'Schedule' : 
        setcurrCard(<Schedule />); 
        setcurrInput('');
        setcurrTitle('Schedule');
        break;
      case 'Parents' : 
        setcurrCard(<Parents />);
        setcurrInput('');
        setcurrTitle('Parents');
        break;
      case 'Venue' : 
        setcurrCard(<Venue />);
        setcurrInput('');
        setcurrTitle('Venue')
        break;
      case 'RSVP' : 
        setcurrCard(<RSVP />); 
        setcurrInput('');
        setcurrTitle('RSVP');
        break ;
      case 'Developer' : 
        setcurrCard(<Developer />); 
        setcurrInput('');
        setcurrTitle('Developer');
        break;
    }

  }


  useEffect(() => {

    if(currTitle != ''){
      
      const present = cards.filter(e => {

        if(e.cardtitle === currTitle){
           return 1;
         } else{
            return 0;
         }
      })


      if(present.length === 0 && cards.length > 0){
        setCards([...cards , {
          cardtitle: currTitle,
          card: currCard
        }])

        if(currInput != ''){
          setInput([...input , {
            input: currInput
          }])  
        }
      } else if(present.length === 1 && cards.length > 0) {

          console.log("here" , present)
          setCards([...cards])

          if(currInput != ''){
            setInput([...input])
          }
         
          
      } else {
        
          console.log('hhhg')
          setCards([{cardtitle: currTitle, card: currCard}]);

          if(currInput != ''){
            setInput([{
              input: currInput
            }])
          }
      }
    }

  } , [currCard])

  useEffect(() => {
      const present = input.filter(e => {
        return e.title === currTitle
      })

      if(currInput !== '')
      {

        if(present.length === 0 && input.length > 0){
          setInput([...input , {
            title: currTitle,
            input : currInput
          }]);


        } else if(present.length === 1 && input.length > 0){
          setInput([...input]);

          
        } else {
            setInput([{
              title: currTitle,
              input: currInput
            }]);
        }
      }

    
  }, [currInput])

  console.log("check" , currInput , input) 

  return (
    <CardContext.Provider value={{ input , cards , addCard , name , setName , f_name , m_name , setFname , setMname }}>
          <div className={styles.sidebar}>
            <Cards />
          </div>
          
          <div className={styles.container}>
            <div className={styles.canva}>
              {
                cards.map((card) => {
                  return card.card
                })
              }
            </div>
          </div>
    </CardContext.Provider>
  )
}

export default Home;