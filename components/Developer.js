import styles from '../styles/Home.module.css'


const Developer = () => {
    return (
        // <div className={styles.card}>
            <div className={styles.bgwhite}>
                        <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" clipRule="evenodd" d="M37.6826 5.27164C35.2196 5.68684 32.6522 6.20224 30.0878 6.84004C27.4418 6.18004 24.7934 5.65924 22.25 5.24104C24.524 4.14184 27.0728 3.52563 29.9336 3.52563C32.8196 3.52683 35.3912 4.15444 37.6826 5.27164Z" fill="#00DBFF"/>
                            <path opacity="0.8" fillRule="evenodd" clipRule="evenodd" d="M37.75 54.7577C35.476 55.8569 32.9272 56.4731 30.0664 56.4731C27.181 56.4731 24.6064 55.8455 22.3174 54.7283C24.7834 54.3131 27.3478 53.7977 29.9122 53.1587C32.5582 53.8163 35.2066 54.3395 37.75 54.7577Z" fill="#008CFF"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M56.4068 3.54956C56.4068 4.04276 56.3846 12.3546 54.7574 22.2504C53.006 18.6282 50.0276 15.7104 46.3574 13.3584C44.0396 9.83996 41.1902 6.98156 37.6826 5.27156C47.3924 3.63716 55.5926 3.55316 56.4068 3.54956Z" fill="#00C3FF"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M6.83979 29.9117C6.17979 32.5577 5.65899 35.2061 5.24079 37.7495C4.14159 35.4755 3.52539 32.9267 3.52539 30.0659C3.52539 27.1805 4.15299 24.6059 5.27019 22.3169C5.68659 24.7829 6.20199 27.3461 6.83979 29.9117Z" fill="#00DBFF"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M56.4508 56.4073C55.9576 56.4073 47.6458 56.3851 37.75 54.7579C41.3722 53.0065 44.29 50.0281 46.642 46.3579C50.1604 44.0401 53.0182 41.1907 54.7288 37.6831C56.3632 47.3929 56.4472 55.5931 56.4508 56.4073Z" fill="#003DFF"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M22.2502 5.24217C18.628 6.99357 15.7102 9.97197 13.3582 13.6422C9.83983 15.96 6.98203 18.8094 5.27143 22.32C3.63703 12.6072 3.55303 4.40697 3.54883 3.59277C4.03363 3.59277 12.349 3.61437 22.2502 5.24217Z" fill="#00FFCE"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M22.3199 54.7283C12.6101 56.3627 4.4099 56.4467 3.5957 56.4509C3.5957 55.9661 3.6173 47.6507 5.2451 37.7495C6.9965 41.3717 9.9749 44.2895 13.6451 46.6415C15.9599 50.1599 18.8093 53.0177 22.3199 54.7283Z" fill="#00C3FF"/>
                            <path opacity="0.8" fillRule="evenodd" clipRule="evenodd" d="M56.4734 29.9341C56.4734 32.8195 55.8458 35.3941 54.7286 37.6831C54.3134 35.2201 53.798 32.6539 53.1602 30.0883C53.8202 27.4423 54.341 24.7939 54.7592 22.2505C55.8602 24.5245 56.4734 27.0733 56.4734 29.9341Z" fill="#008CFF"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M13.3583 13.6399C10.3583 18.3157 8.27988 24.1105 6.83988 29.9119C6.20208 27.3463 5.68668 24.7831 5.27148 22.3201C6.98208 18.8095 9.83988 15.9601 13.3583 13.6399Z" fill="#00FFF8"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M54.7284 37.6829C53.0178 41.1905 50.16 44.0399 46.6416 46.3601C49.6416 41.6843 51.7188 35.8895 53.1588 30.0881C53.7978 32.6537 54.3132 35.2199 54.7284 37.6829Z" fill="#0086FF"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M13.6402 46.6417C9.97243 44.2903 6.99403 41.3719 5.24023 37.7497C5.66023 35.2063 6.18163 32.5579 6.83923 29.9119C8.33203 35.8885 10.4956 41.8711 13.6402 46.6417Z" fill="#00BDFF"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M29.9116 53.16C27.3472 53.799 24.7828 54.3144 22.3168 54.7296C18.8092 53.0178 15.9598 50.16 13.6396 46.6416C18.3154 49.6416 24.1102 51.72 29.9116 53.16Z" fill="#03BDE8"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M30.0882 6.83993C24.1116 8.33033 18.129 10.4939 13.3584 13.6385C15.7098 9.97073 18.6282 6.99233 22.2504 5.23853C24.7938 5.66033 27.4422 6.18353 30.0882 6.83993Z" fill="#00FFF8"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M46.6419 46.3601C44.2905 50.0279 41.3721 53.0063 37.7499 54.7601C35.2065 54.3401 32.5581 53.8187 29.9121 53.1611C35.8887 51.6683 41.8713 49.5047 46.6419 46.3601Z" fill="#0086FF"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M54.758 22.2504C54.338 24.7938 53.8166 27.4422 53.159 30.0882C51.6686 24.1116 49.505 18.129 46.3604 13.3584C50.0282 15.7098 53.0066 18.6282 54.758 22.2504Z" fill="#03BDE8"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M46.3599 13.3583C41.6841 10.3583 35.8893 8.27988 30.0879 6.83988C32.6523 6.20208 35.2197 5.68668 37.6827 5.27148C41.1903 6.98148 44.0397 9.83988 46.3599 13.3583Z" fill="#00BDFF"/>
                            <path fillRule="evenodd" clipRule="evenodd" d="M46.8002 30.0628C45.756 34.271 44.2492 38.4746 42.0734 41.869C38.6131 44.1496 34.2733 45.719 29.937 46.7999C25.7276 45.7533 21.5237 44.2466 18.1315 42.071C15.8508 38.611 14.2812 34.2715 13.2002 29.9355C14.2444 25.7264 15.7512 21.5228 17.9269 18.1309C21.3873 15.8503 25.7271 14.2809 30.0633 13.2C34.2728 14.244 38.4767 15.7508 41.8714 17.9263C44.1496 21.3874 45.7192 25.7269 46.8002 30.0628Z" fill="url(#paint0_linear)"/>
                            <defs>
                            <linearGradient id="paint0_linear" x1="40.4958" y1="40.4026" x2="18.7232" y2="18.8173" gradientUnits="userSpaceOnUse">
                            <stop stopColor="#00E7FF"/>
                            <stop offset="1" stopColor="#0043FF"/>
                            </linearGradient>
                            </defs>
                        </svg>
                        <div>
                            <p>ROVAE Inc.</p>
                            <p>#ITmadesimple</p>
                        </div>
            </div>
            
        // </div>
    )
}

export default Developer
