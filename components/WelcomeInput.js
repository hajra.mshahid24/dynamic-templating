import React, { useContext } from 'react'
import { CardContext } from './Home'

const WelcomeInput = () => {

    const {name , setName} = useContext(CardContext);
    return (
        <div style={{margin: '1rem 0.5rem'}}>
            <h2><strong>Name</strong></h2>
            <input value={name} onChange={ e => setName(e.target.value)} />
        </div>
    )
}

export default WelcomeInput
