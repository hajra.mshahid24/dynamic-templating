import React, { useContext } from 'react'
import { CardContext } from './Home'

const InviteInput = () => {
    const {f_name , m_name , setMname , setFname} = useContext(CardContext);
    return (
        <div style={{margin: '1rem 0.5rem'}}>
            {/* <h2><strong></strong></h2> */}
            <label><strong>Father's name</strong></label>
            <input className="mb-4" value={f_name} onChange={ e => setFname(e.target.value)} />
            <label><strong>Mother's name</strong></label>
            <input className="md-4" value={m_name} onChange={ e => setMname(e.target.value)} />
        </div>
    )
}

export default InviteInput
