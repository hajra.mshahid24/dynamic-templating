import styles from '../styles/Home.module.css'


const Schedule = () => {
    return (
        // <div className={styles.card}>
            <div className={`${styles.bgwhite} ${styles.sed}`}>
                        <div className={styles.sed__items}>
                            <h3 className="font-semibold font-inter text-primary">Ring Ceremony</h3>
                            <p className="font-inter text-primary">On July 11, from 10:30 am onw..</p>
                        </div>
                        <div className={styles.sed__items}>
                            <h3 className="font-semibold font-inter text-primary">Mandap Muhurat & Holi-H..</h3>
                            <p className="font-inter text-primary">On July 11, from 12:30 pm onw..</p>
                        </div>
                        <div className={styles.sed__items}>
                            <h3 className="font-semibold font-inter text-primary">Mocktail Party</h3>
                            <p className="font-inter text-primary">On July 11, from 7:30 pm onwa..</p>
                        </div>
                        <div className={styles.sed__items}>
                            <h3 className="font-semibold font-inter text-primary">Sangeet Sandhaya</h3>
                            <p className="font-inter text-primary">On July 12, from 7:30 pm onw..</p>
                        </div>
                        <div className={styles.sed__items}>
                            <h3 className="font-semibold font-inter text-primary">Wedding</h3>
                            <p className="font-inter text-primary">On July 13, from 10:30 am on..</p>
                        </div>
                    {/* </div> */}
            </div>
            
        // </div>
    )
}

export default Schedule
